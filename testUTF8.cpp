﻿// // Simulation.cpp : définit le point d'entrée pour l'application console.
// //

// //Includes standards
// #include <cstdlib>
// #include <ctime>
// #include <iostream> 
// #include <iomanip>


// #include <fstream>
// #include <sstream>
// #include <string.h>
// #include <sstream>
// #include <random>
// #include <algorithm>
// #include <vector>
// #include <utility>
// #include <random>
// #include <numeric>
// #include <stdexcept>
// #include <functional>
// #include <iterator> 
// //Includes tiers

// #include "termcolor.hh"

// //Nos classes

// #include "suite.hh"


// using namespace placeholders;
// using namespace std;
// // FONCTIONS

// //typedef vector<int> Suite<>;

// template <class ForwardIterator>
// void writeContainer(ostream& out, ForwardIterator begin, ForwardIterator end,
// 					string const prefix, string const infix, string const suffix){
// 	if(begin!=end){
// 		out << prefix << *begin;
// 		++begin;
// 		for_each(begin,end,[&](typename ForwardIterator::reference value){out << infix << value;});
// 		out << suffix;
// 	}
// }

// template <class Container>
// void writeContainer(ostream& out, Container const container, 
// 					string const prefix, string const infix, string const suffix){
// 	writeContainer(out,begin(container),end(container),prefix,infix,suffix);
// }

// enum Automate{
// 		ADD,SUB
// };

// enum AutoSide{
// 	ANTECEDENT,IMAGE
// };



// struct Motif{
// 	Suite<> suite; 
// 	// Contient les indices des frontières des sous-parties
// 	// (y compris les frontières triviales au début et à la fin)
// 	vector<pair<int,int>> indices;
// 	vector<Motif> extra;
// 	string nom;
// };

// string genLatex(vector<Suite<>> const & tab, int TailleF)
// {
// 	stringstream output;
// 	for(Suite<> const & suite:tab){
// 		writeContainer(output,begin(suite),begin(suite)+TailleF,"","&","\\\\ \n");
// 	}
// 	return output.str();
// }

// // Donne l'ordre additif d'un élément dans le groupe cyclique de taille donnée
// int ordre(int tailleGroupe, int element) {
// 	return tailleGroupe / gcd(tailleGroupe,element);
// }

// // Somme les éléments d'un conteneur d'entiers
// template <class Container>
// int somme(Container const & container){
// 	return reduce(begin(container),end(container),0);
// }

// // Somme les entiers de la plage définie par les itérateurs
// // Juste un cas particulier avec un nom plus explicite de reduce
// template<class ForwardIterator>
// int somme(ForwardIterator begin, ForwardIterator end){
// 	return reduce(begin,end,0);
// }

// // Donne les coefficients des valeurs initiales de antécédents pour le calcul du caractéristique
// vector<int> invPoidsA(int profondeur,int periode, Automate automate){
// 	vector<int> poids(profondeur);
// 	generate(rbegin(poids),rend(poids),[&,j=0]() mutable {
// 		// Le signe est toujours positif si on est sur delta, sinon il dépende de la parité de periode et j
// 		int signe = (automate == Automate::ADD) && (periode+j)%2 ? -1 : 1;
// 		return signe * binomialCoeff(periode,j++);
// 	});
// 	poids.back()-=1; // Soustraire a_profondeur pour le nombre caractéristique
// 	return move(poids);
// }

// // Donne les coefficients des valeurs de la suite pour le calcul du caractéristique
// vector<int> invPoidsSuite(int profondeur,int periode, Automate automate){
// 	vector<int> poids(max(periode-profondeur+1,0));
// 	generate(begin(poids),end(poids),[&,j=0]() mutable {
// 		// Le signe est toujours positif si on est sur delta, sinon il dépende de la parité de periode, profondeur et j
// 		int signe = ((automate == Automate::ADD) && (periode+j+profondeur)%2) ? -1 : 1;
// 		return signe * binomialCoeff(periode-j++-1,profondeur-1);
// 	});
// 	return move(poids);
// }

// int periodeInv(int profondeur, vector<int> const & A, int tailleGroupe, Suite<> const & suite, Automate automate){
// 	// On regarde la période à l'itération précédente
// 	int periodePrec = (profondeur == 1) ? 
// 					  size(suite) :
// 					  periodeInv(profondeur-1,A,tailleGroupe,suite, automate);

// 	// Le nombre caractéristique est le décalage sur une période de l'antécédent précédent
// 	// Il est toujours donné par une somme pondérée des valeurs du vecteur A et des valeurs de la suite
// 	// On s'en réfère à des fonctions auxiliaires pour obtenir les poids
// 	vector<int> poidsA = invPoidsA(profondeur,periodePrec,automate);
// 	vector<int> poidsSuite = invPoidsSuite(profondeur,periodePrec,automate);

// 	// inner_product permet une somme pondérée
// 	int caracteristique = std::inner_product(begin(poidsA),end(poidsA),begin(A),0)
// 						+ std::inner_product(begin(poidsSuite),end(poidsSuite),begin(suite),0);
	
// 	// D'après les résultats du papier
// 	if(automate == Automate::ADD && (periodePrec % 2) && (caracteristique % tailleGroupe)){
// 		// Cas particulier : période paire pour l'automate additif avec un nombre caractéristique différent de 0
// 		return 2*periodePrec;
// 	}else{
// 		int q = ordre(tailleGroupe, caracteristique);
// 		return q*periodePrec;
// 	}
// }

// bool isTransposededStart(Suite<> const & pattern, Suite<> const & tab, int start, int ordre ){
// 	if(std::empty(pattern)) return false; // Cas spécial : pattern vide
// 	int diff = modulo(tab[start]-pattern[0],ordre);
// 	// On utilise une version modifiée d'égalité : égal si la différence est constante
// 	return equal(begin(pattern)+1,end(pattern),begin(tab)+start+1,
// 				 [&](Suite<>::value_type a, Suite<>::value_type b){return modulo(a-b,ordre) == diff;} 
// 	);
// }

// Suite<> addSuite(Suite<> const & suite, int a, int ordre)
// {
// 	Suite<> res(size(suite)); 
// 	transform(begin(suite),end(suite),begin(res),[&](Suite<>::value_type value){return value+a;});
// 	return res; 
// }


// // Renvoie une copie avec une permutation cyclique
// template <class Container>
// Container circularShift(Container container, int k) // Noter le passage par copie de container
// {
// 	// rotate fait une permutation circulaire
// 	rotate(begin(container),begin(container)+k,end(container));
// 	return container;
// }


// Motif initSuitePeriodiqueRandom(int periode,int card) {
// 	std::random_device rd;  //Will be used to obtain a seed for the random number engine
//     std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
// 	std::uniform_int_distribution<int> distrib(0,card-1);

// 	Motif randomMotif;
// 	randomMotif.suite=Suite<>(periode);
// 	generate_n(begin(randomMotif.suite),periode,[&](){return distrib(gen);});
// 	randomMotif.indices={{0,periode}};

// 	return move(randomMotif);
// }

// template <class Container>
// vector<pair<typename Container::value_type,typename Container::value_type>> groupByPair(Container ungrouped){
// 	vector<pair<typename Container::value_type,typename Container::value_type>> result;
// 	for(auto it = begin(ungrouped);it!=end(ungrouped);){
// 		auto a = *it++;
// 		auto b = *it++;
// 		result.push_back(make_pair(a,b));
// 	}
// 	return move(result);
// }


// vector<pair<int,int>> getSubRanges(int taillePattern){	
// 	int count=1;
// 	std::cout<< "Combien de parties pour votre thème ? ";
// 	cin>> count;

// 	if(count>1){
// 		//TODO: vérifier la validité des valeurs
// 		std::cout << "Veuillez entrer les indices de début (inclus) et fin (exclus) de vos parties : "; 
// 		vector<int> buffer(2*count);
// 		copy_n(istream_iterator<int>(cin),2*count,begin(buffer));
// 		return groupByPair(buffer);
// 	}else{
// 		return {{0,taillePattern}};
// 	}
// }

// Suite<> promptSuite(int tailleSuite, int card){
// 	std::cout << "Veuillez rentrer les valeurs de la suite periodique : ";
// 	Suite<> result(tailleSuite);
// 	copy_n(istream_iterator<int>(std::cin), tailleSuite, begin(result));
// 	transform(begin(result),end(result),begin(result),[&](int a){return modulo(a,card);});
// 	return move(result);
// }

// Suite<> computeImage(Suite<> const & suite, Automate automate, int card);
// Suite<> computeAntecedent(Suite<> const & suite, Automate automate, bool randomAntecedent, int card);

// Motif initSuitePeriodique(int periode, int card) {
// 	//TODO: séparer entrée de séquence et entrée de sous-motifs
// 	Motif result;
// 	result.suite=promptSuite(periode,card);
// 	// result.indices=vector<int>(segment+1,0);
	

// 	result.indices = getSubRanges(periode);
// 	result.nom = string("Principale");
// 		// vector <int> taille(segment);
// 		// std::cout << "Veuillez entrer les tailles de vos parties : "; 
// 		// copy_n(istream_iterator<int>(std::cin),segment,taille.begin());
// 		// std::partial_sum(taille.begin(),taille.end(),result.indices.begin()+1);

// 	int extraMotifs;
// 	std::cout << "Désirez vous rechercher des motifs supplémentaires ? (0: Non, 1: Delta, 2: Tau, 3: Les deux, 4: Personnalisé)";
// 	std::cin >> extraMotifs;
// 	if(extraMotifs & 1){ // On veut Delta
// 		int direction;
// 		std::cout << "Quelle direction pour Delta ? (0: Annuler, 1: Image, 2: Antecedent, 3: Les deux)";
// 		std::cin >> direction;
// 		if(direction & 1){
// 			Motif extra;
// 			extra.suite = computeImage(result.suite,Automate::SUB,card);
// 			cout << "Sequence : ";
// 			writeContainer(cout,extra.suite,""," ","\n");
// 			extra.indices = getSubRanges(size(extra.suite));
// 			extra.nom = string("Delta");
// 			result.extra.push_back(extra);
// 		}
// 		if(direction & 2){
// 			Motif extra;
// 			extra.suite = computeAntecedent(result.suite,Automate::SUB,false,card);
// 			cout << "Sequence : ";
// 			writeContainer(cout,extra.suite,""," ","\n");
// 			extra.indices = getSubRanges(size(extra.suite));
// 			extra.nom = string("DeltaInv");
// 			result.extra.push_back(extra);
// 		}
// 	}
// 	if(extraMotifs & 2){ // On veut Tau
// 		int direction;
// 		std::cout << "Quelle direction pour Tau ? (0: Annuler, 1: Image, 2: Antecedent, 3: Les deux)";
// 		std::cin >> direction;
// 		if(direction & 1){
// 			Motif extra;
// 			extra.suite = computeImage(result.suite,Automate::ADD,card);
// 			cout << "Sequence : ";
// 			writeContainer(cout,extra.suite,""," ","\n");
// 			extra.indices = getSubRanges(size(extra.suite));
// 			extra.nom = string("Tau");
// 			result.extra.push_back(extra);
// 		}
// 		if(direction & 2){
// 			Motif extra;
// 			extra.suite = computeAntecedent(result.suite,Automate::ADD,false,card);
// 			cout << "Sequence : ";
// 			writeContainer(cout,extra.suite,""," ","\n");
// 			extra.indices = getSubRanges(size(extra.suite));
// 			extra.nom = string("TauInv");
// 			result.extra.push_back(extra);
// 		}
// 	}
// 	if(extraMotifs & 4){ // On veut du personnalisé
// 		bool cont;
// 		int extraCount = 1;
// 		do{
// 			Motif extra;
// 			int taille;
// 			cout << "Quelle taille ? ";
// 			cin >> taille;
// 			extra.suite = promptSuite(taille,card);
// 			extra.indices = getSubRanges(size(extra.suite));
// 			extra.nom = string("Extra") + to_string(extraCount++);
// 			result.extra.push_back(extra);
// 			cout << "Encore un ? (0: Non, 1: Oui) ";
// 			cin >> cont;
// 		}while(cont);
// 	}
// 	return move(result);
// }

// // Renvoie une copie de la suite avec les valeurs opposée
// Suite<> oppose(Suite<> suite, int tailleGroupe){
// 	transform(begin(suite),end(suite),begin(suite),
// 			  [&](Suite<>::value_type el){return modulo(-el, tailleGroupe);});
// 	return move(suite);
// }

// vector <int> ShiftPermutationDetector(Suite<> const & pattern, Suite<> const & suite, int N ) // détecte les permutations du motif ou d'une transposition du motif initial
// {
// 	vector<int> compte(N,0); 
// 	vector<int> res; 
// 	for (int i =0 ; i < pattern.size(); i++ )
// 	{
// 		compte[pattern[i]] = compte[pattern[i]] +1;
// 	}
// 	vector <int> test(N,0);
// 	for (int i =0; i < suite.size() ; i++) 
// 	{
// 		if (i==0) // Il faut initialiser test
// 		{
// 			for (int j =0 ; j < pattern.size(); j++ )
// 			{
// 				test[suite[j]] = test[suite[j]] +1;
// 			}
// 		}
// 		else{ // On met à jour test
// 			test[suite[i-1]] --;
// 			test[suite[(i-1+pattern.size())%suite.size()]]++;
// 		}

// 		for(int j = 0; j < N; j++)
// 		{
// 			vector<int> shifted = circularShift(test,j);
// 			if ( equal(shifted.begin(),shifted.end(),compte.begin(),compte.end())   )
// 			{ res.push_back(i); }
// 		}
// 	}
// 	return res;
// }

// vector <int> ShiftPermutationComplementaireDetector(Suite<> const & pattern, Suite<> const & tab, int N ) // détecte les permutations du motif ou d'une transposition du motif initial
// {
// 	Suite<> complementaire=oppose(pattern,N);
// 	return ShiftPermutationDetector(complementaire,tab,N) ; 
// }

// int computeImage(Automate automate, int oper1, int oper2, int card){
// 	switch(automate){
// 		case Automate::ADD :
// 			return modulo(oper1+oper2,card);
// 		case Automate::SUB :
// 			return modulo(oper1-oper2,card);
// 		default:
// 			cerr << "Unsupported automata" << endl;
// 			return 0;
// 	};
// }

// Suite<> computeImage(Suite<> const & suite, Automate automate, int card){
// 	Suite<> result(size(suite));
// 	auto f = [&](int a, int b){return modulo(automate == Automate::SUB ? a-b : a+b,card);};
// 	adjacent_difference(begin(suite),end(suite)+1,begin(result)-1,f);
	
// 	result.reduce();
// 	return move(result);
// }

// Suite<> computeAntecedent(Suite<> const & suite, Automate automate, bool randomAntecedent, int card){
// 	int first;
// 	if(randomAntecedent){
// 		std::random_device rd;  //Will be used to obtain a seed for the random number engine
// 		std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
// 		std::uniform_int_distribution<int> distrib(0,card-1);
// 		first = distrib(gen); //TODO: better random
// 	}else{
// 		std::cout << "Valeur initiale ? " << endl;
// 		cin >> first;
// 	}

// 	int newPeriod = periodeInv(1,{first},card,suite,automate);
// 	vector<int> helper(newPeriod,first);
// 	copy_n(begin(suite),newPeriod-1,next(begin(helper)));
// 	Suite<> result(newPeriod);
// 	auto f = [&](int a, int b){return modulo(automate == Automate::SUB ? a+b : b-a,card); };
// 	partial_sum(begin(helper),end(helper),begin(result),f);
// 	return result;
// }


// void computeTab(vector<Suite<>>& tab, int profondeur, int card, Automate automate, 
// 				AutoSide automateSide, bool randomAntecedent){
// 	for (int k = 1; k < profondeur; k++) // k nous dit à quel niveau de récursion on se situe 
// 	{
// 		if(automateSide == AutoSide::ANTECEDENT){
// 			tab[k] = computeAntecedent(tab[k-1],automate,randomAntecedent,card);
// 		}else{
// 			tab[k] = computeImage(tab[k-1],automate,card);
// 		}
// 	}
// }

// // Functions to get vectors of iterators from a container of containers.
// template<class Container> 
// auto multiBegin(Container const & container) -> vector<decltype(begin(container.front()))> {
// 	vector<decltype(begin(container.front()))> result(size(container));
// 	transform(begin(container),end(container),begin(result),std::begin);
// 	return result;
// }

// template<class Container> 
// auto multiEnd(Container const & container) -> vector<decltype(begin(container.front()))> {
// 	vector<decltype(begin(container.front()))> result(size(container));
// 	transform(begin(container),end(container),begin(result),std::begin);
// 	return result;
// }

// // Here a range is simply a pair of begin/end iterators
// template<class Container> 
// auto multiRange(Container const & container) -> vector<pair<decltype(begin(container.front())),decltype(end(container.front()))>> {
// 	vector<pair<decltype(begin(container.front())),decltype(end(container.front()))>> result(size(container));
// 	transform(begin(container),end(container),begin(result),
// 		[&](auto& elem){return make_pair(begin(elem),end(elem));});
// 	return result;
// }

// // NYI
// // class Checker{
// // 	private:
// // 	int position;
// // 	vector<pair<vector<pair<int,int>>::iterator,vector<int>::iterator>> ranges;
// // 	vector<pair<Motif,int>> lastMatches;
// // 	public:
// // 	Checker(vector<pair<Motif,vector<int>>> const & motifs){

// // 	}

// // 	bool checkNext();
// // 	vector<pair<Motif,int>> getLastMatches();
// // };

// double distanceSuite(Suite<> const & a, Suite<> const & b, int tailleGroupe){
// 	if(size(a)!=size(b)){
// 		throw "Distance pas implémentée pour des suites de tailles différentes";
// 	}else{
// 		double sum = 0.0;
// 		for(auto it_a = begin(a),it_b = begin(b); it_a != end(a);++it_a,++it_b){
// 			sum += min(abs(modulo(*it_a-*it_b,tailleGroupe)),abs(modulo(*it_b-*it_a,tailleGroupe)));
// 		}
// 		// Régularisation
// 		sum /= (tailleGroupe/2) * size(a);
// 		return sum;
// 	}
// }

// Suite<> transpose(Suite<> suite, int shift, int tailleGroupe){
// 	for(int & value : suite){
// 		value = modulo(value+shift,tailleGroupe);
// 	}
// 	return move(suite);
// }

// double distanceSuiteTransposition(Suite<> const & a, Suite<> const & b, int tailleGroupe, double penaliteTransposition = 0.1){
// 	double result = distanceSuite(a,b,tailleGroupe);
// 	for(int i = 1; i<tailleGroupe;i++){
// 		double d = distanceSuite(a,transpose(b,i,tailleGroupe),tailleGroupe);
// 		if(d + penaliteTransposition < result){
// 			result = d + penaliteTransposition;
// 		}
// 	}
// 	return result;
// }

// // Renvoie les positions de haystack où se trouve des occurrences approchées (au max à une distance de plafond) de needle
// vector<pair<int,double>> distanceTranspositionMatcher(Suite<> const & needle, Suite<> const & haystack, int tailleGroupe, double plafond){
// 	vector<pair<int,double>> result;
// 	for(auto iter = begin(haystack);iter!= end(haystack);++iter){
// 		double d = distanceSuiteTransposition(needle,Suite<>(iter,iter+size(needle)),tailleGroupe);
// 		if(d<=plafond){
// 			result.push_back(make_pair(distance(begin(haystack),iter),d));
// 		}
// 	}
// 	return move(result);
// }

// auto findSousMotifs(Motif const & motif, Suite<> const & suite, int tailleGroupe){
// 	typedef decltype(ShiftPermutationDetector(suite, suite, tailleGroupe)) Match;
// 	//typedef decltype(motif.indices.front()) SubMotif;
// 	//vector<pair<SubMotif,Match>> result;
// 	vector<Match> result;
// 	for(auto subRange : motif.indices)
// 	{
// 		Suite<> suiteTest(begin(motif.suite)+subRange.first,begin(motif.suite)+subRange.second);
// 		//result.push_back(make_pair(subRange,ShiftPermutationDetector(suiteTest, suite, tailleGroupe))) ; 
// 		result.push_back(ShiftPermutationDetector(suiteTest, suite, tailleGroupe)); 
// 	}
// 	return move(result);
// }

// auto findSousMotifsApprox(Motif const & motif, Suite<> const & suite, int tailleGroupe, double plafond){
// 	typedef decltype(distanceTranspositionMatcher(suite, suite, tailleGroupe, plafond)) Match;
// 	//typedef decltype(motif.indices.front()) SubMotif;
// 	//vector<pair<SubMotif,Match>> result;
// 	vector<Match> result;
// 	for(auto subRange : motif.indices)
// 	{
// 		Suite<> suiteTest(begin(motif.suite)+subRange.first,begin(motif.suite)+subRange.second);
// 		//result.push_back(make_pair(subRange,ShiftPermutationDetector(suiteTest, suite, tailleGroupe))) ; 
// 		result.push_back(distanceTranspositionMatcher(suiteTest, suite, tailleGroupe, plafond)); 
// 	}
// 	return move(result);
// }

// template <class SubContainer>
// SubContainer flatten(vector<SubContainer> const & all){
// 	SubContainer result;
// 	for(SubContainer const & one : all){
// 		result.insert(end(result),begin(one),end(one));
// 	}
// 	return move(result);
// }

// void afficheSuite(Suite<> const & suite, Motif& motif,int tailleGroupe, double plafond = 0.2, bool detailed = true){
// 	// Motif principal
// 	//vector<int> shiftPermutations=ShiftPermutationDetector(motif.suite, suite, tailleGroupe);
// 	vector<pair<int,double>> shiftPermutations=distanceTranspositionMatcher(motif.suite, suite, tailleGroupe,plafond);
// 	auto checkPermutation = [&,permIt=begin(shiftPermutations),permSent=end(shiftPermutations)](int index) mutable {
// 		if(permIt != permSent && index == (*permIt).first){
// 			++permIt;
// 			return true;
// 		}else{
// 			return false;
// 		}
// 	};

// 	int l;
// 	if(false){
// 	// Sous-motifs
// 	vector<vector<pair<int,double>>> distanceSousMotif = findSousMotifsApprox(motif,suite,tailleGroupe,plafond);
// 	auto checkSousPermutation = [&,permRanges=multiRange(distanceSousMotif)](int index) mutable {
// 		bool result = false;
// 		for(auto& range : permRanges){
// 			if (range.first != range.second && index == (*range.first).first){
// 				result = true;
// 				++range.first;
// 			}
// 		}
// 		return result;
// 	};
// 	vector<vector<int>> ShiftPermutationsSousMotif;
// 	for(auto subRange : motif.indices)
// 	{
// 		Suite<> suiteTest(begin(motif.suite)+subRange.first,begin(motif.suite)+subRange.second);
// 		ShiftPermutationsSousMotif.push_back(ShiftPermutationDetector(suiteTest, suite, tailleGroupe)) ; 
// 	}
// 	for(auto it:ShiftPermutationsSousMotif){
// 		writeContainer(cout,it,""," ","\n");
// 	}

// 	// Motifs alternatifs
// 	vector<vector<vector<pair<int,double>>>> altMotifs;
// 	for(Motif subMotif : motif.extra){
// 		altMotifs.push_back(findSousMotifsApprox(subMotif,suite,tailleGroupe,plafond));
// 		// for(auto& subRange : subMotif.indices)
// 		// {
// 		// 	Suite<> suiteTest(begin(subMotif.suite)+subRange.first,begin(subMotif.suite)+subRange.second);
// 		// 	altMotifs.push_back(ShiftPermutationDetector(suiteTest, suite, tailleGroupe)) ; 
// 		// }
// 	}
// 	auto checkAltDistance = [&,permRanges=multiRange(flatten(altMotifs))](int index) mutable {
// 		bool result = false;
// 		for(auto range : permRanges){
// 			if (range.first != range.second && index == (*range.first).first){
// 				result = true;
// 				++range.first;
// 			}
// 		}
// 		return result;
// 	};

// 	vector<vector<vector<int>>> altMotifsPerm;
// 	for(Motif subMotif : motif.extra){
// 		altMotifsPerm.push_back(vector<vector<int>>());
// 		for(auto& subRange : subMotif.indices)
// 		{
// 			Suite<> suiteTest(begin(subMotif.suite)+subRange.first,begin(subMotif.suite)+subRange.second);
// 			altMotifsPerm.back().push_back(ShiftPermutationDetector(suiteTest, suite, tailleGroupe)) ; 
// 		}
// 	}
// 	auto checkAltPermutation = [&,permRanges=multiRange(flatten(altMotifsPerm))](int index) mutable {
// 		bool result = false;
// 		for(auto range : permRanges){
// 			if (range.first != range.second && index == (*range.first)){
// 				result = true;
// 				++range.first;
// 			}
// 		}
// 		return result;
// 	};
// 	// vector<int> ShiftComplementairePermutations=ShiftPermutationDetector(oppose(Motif,N), tab[k],N );

// 	//Affiche motifs alt distance
// 	l = 0;
// 	for(int value : suite) {
// 		if(checkAltDistance(l++)){
// 			std::cout << termcolor::yellow;
// 		}
// 		std::cout << "|" << setw(2) << value << "|" << termcolor::reset;
// 	}
// 	std::cout  << "----" << shiftPermutations.size() << " " << size(suite) << endl;
// 	if(detailed){
// 		for(int i = 0; i < size(motif.extra); i++){
// 			Motif const & subMotif = motif.extra[i]; 
// 			for(int j = 0; j < size(subMotif.indices);j++){
// 				for(auto match : altMotifs[i][j]){
// 					cout << "Sous-motif de " << subMotif.nom;
// 					writeContainer(cout,begin(subMotif.suite)+subMotif.indices[j].first,
// 							       begin(subMotif.suite)+subMotif.indices[j].second," ("," ",")");
// 				 	cout << " en position " << match.first << " sous la forme : ";
// 					writeContainer(cout,begin(suite)+match.first,begin(suite)+match.first+(subMotif.indices[j].second-subMotif.indices[j].first),
// 								   ""," "," ");
// 					cout << (match.second ? termcolor::reset : termcolor::green) << "(distance:" << match.second << ")" << termcolor::reset << endl; 
// 				}
// 			}
// 		}
// 	}

// 	// Affiche motifs alt perm
// 	l = 0;
// 	for(int value : suite) {
// 		if(checkAltDistance(l++)){
// 			std::cout << termcolor::yellow;
// 		}
// 		std::cout << "|" << setw(2) << value << "|" << termcolor::reset;
// 	}
// 	std::cout  << "----" << shiftPermutations.size() << " " << size(suite) << endl;
// 	if(detailed){
// 		for(int i = 0; i < size(motif.extra); i++){
// 			Motif const & subMotif = motif.extra[i]; 
// 			for(int j = 0; j < size(subMotif.indices);j++){
// 				for(int match : altMotifsPerm[i][j]){
// 					cout << "Sous-motif de " << subMotif.nom;
// 					writeContainer(cout,begin(subMotif.suite)+subMotif.indices[j].first,
// 							       begin(subMotif.suite)+subMotif.indices[j].second," ("," ",")");
// 				 	cout << " en position " << match << " sous la forme : ";
// 					writeContainer(cout,begin(suite)+match,begin(suite)+match+(subMotif.indices[j].second-subMotif.indices[j].first),
// 								   ""," ","\n"); 
// 				}
// 			}
// 		}
// 	}

// 	//Affiche sous motifs distance
// 	l = 0;
// 	for(int value : suite) {
// 		if(checkSousPermutation(l++)){
// 			std::cout << termcolor::green;
// 		} 
// 		std::cout << "|" << setw(2) << value << "|" << termcolor::reset;
// 	}
// 	std::cout  << "----" << shiftPermutations.size() << " " << size(suite) << endl;
// 	if(detailed){
// 		Motif const & subMotif = motif; 
// 		for(int j = 0; j < size(subMotif.indices);j++){
// 			for(auto match : distanceSousMotif[j]){
// 				cout << "Sous-motif de " << subMotif.nom;
// 				writeContainer(cout,begin(subMotif.suite)+subMotif.indices[j].first,
// 								begin(subMotif.suite)+subMotif.indices[j].second," ("," ",")");
// 				cout << " en position " << match.first << " sous la forme : ";
// 				writeContainer(cout,begin(suite)+match.first,begin(suite)+match.first+(subMotif.indices[j].second-subMotif.indices[j].first),
// 								""," "," ");
// 				cout << (match.second ? termcolor::reset : termcolor::green) << "(distance:" << match.second << ")" << termcolor::reset << endl;
// 			}
// 		}
// 	}

// 	//Affiche sous-motifs perm
// 	l = 0;
// 	for(int value : suite) {
// 		if(checkSousPermutation(l++)){
// 			std::cout << termcolor::green;
// 		} 
// 		std::cout << "|" << setw(2) << value << "|" << termcolor::reset;
// 	}
// 	std::cout  << "----" << shiftPermutations.size() << " " << size(suite) << endl;
// 	if(detailed){
// 		Motif const & subMotif = motif; 
// 		for(int j = 0; j < size(subMotif.indices);j++){
// 			for(auto match : ShiftPermutationsSousMotif[j]){
// 				cout << "Sous-motif de " << subMotif.nom;
// 				writeContainer(cout,begin(subMotif.suite)+subMotif.indices[j].first,
// 								begin(subMotif.suite)+subMotif.indices[j].second," ("," ",")");
// 				cout << " en position " << match << " sous la forme : ";
// 				writeContainer(cout,begin(suite)+match,begin(suite)+match+(subMotif.indices[j].second-subMotif.indices[j].first),
// 								""," ","\n");
// 				//cout << (match.second ? termcolor::reset : termcolor::green) << "(distance:" << match.second << ")" << termcolor::reset << endl;
// 			}
// 		}
// 	}
// 	}

// 	l=0;
// 	for(int value : suite) {
// 		if(checkPermutation(l++)){
// 			std::cout << termcolor::red;
// 		}
// 		std::cout << "|" << setw(2) << value << "|" << termcolor::reset;
// 	}
// 	std::cout  << "----" << shiftPermutations.size() << " " << size(suite) << endl;
// }
	



// void afficheTab(vector<Suite<>>& tab, Motif& motif,int tailleGroupe)
// {
// 	for(Suite<> const & suite : tab)
// 	{
// 		afficheSuite(suite, motif, tailleGroupe);
// 	}
// 	std::cout << endl;
// }




// struct Parameters{
// 	int TailleF;
// 	int profondeur;
// 	int card;
// 	int periode;
// 	bool choix;
// 	Automate automate;
// 	AutoSide automateSide;
// 	bool randomAntecedent;
// };

// Parameters readCLParameters(int argc,char ** argv){
// 	Parameters pars;
// 	if(argc < 2){
// 		std::cout << "Taille de fenetre:  " ;  
// 		cin >> pars.TailleF;	
// 	}else{
// 		pars.TailleF = atoi(argv[1]);
// 	}
	
// 	if(argc < 3){
// 		std::cout << "Profondeur iterative:  ";
// 		cin >> pars.profondeur;
// 	}else{
// 		pars.profondeur = atoi(argv[2]);
// 	}

// 	if(argc < 4){
// 		std::cout << "Alphabet:  " ;
// 		cin >> pars.card;
// 	}else{
// 		pars.card = atoi(argv[3]);
// 	}

// 	if(argc < 5){
// 		std::cout << "Periode:  ";
// 		cin >> pars.periode;		
// 	}else{
// 		pars.periode = atoi(argv[4]);
// 	}

// 	if(argc < 6){
// 		std::cout << "Deterministe 0 aleatoire 1:  " ;
// 		cin >> pars.choix;
// 	}else{
// 		pars.choix = atoi(argv[5]);
// 	}

// 	string automateName;
// 	if(argc<7){
// 		std::cout << "Automate (+,-,+-) : " ;
// 		cin >> automateName;
// 	}else{
// 		automateName = string(argv[6]);
// 	}
// 	if			(automateName == string("+")){
// 		pars.automate=Automate::ADD;
// 		std::cout << "Automate additif" << endl;
// 	}else if	(automateName == string("-")){
// 		pars.automate=Automate::SUB;
// 		std::cout << "Automate soustractif" << endl;
// 	}else{
// 		std::cout << "Automate non reconnu" << endl;
// 		exit(1);
// 	}

// 	string sideString;
// 	if(argc<8){
// 		std::cout << "Direction des itérations ('+','-'): " ;
// 		cin >> sideString;
// 	}else{
// 		sideString = argv[7];
// 	}
// 	if(sideString == "-"){
// 		pars.automateSide = AutoSide::ANTECEDENT;
// 	}else if (sideString == "+"){
// 		pars.automateSide = AutoSide::IMAGE;
// 	}else{
// 		std::cout << "Direction non reconnu" << endl;
// 	}
	
// 	if(pars.automateSide == AutoSide::ANTECEDENT){
// 		if(argc<9){
// 			std::cout << "Antécédent aléatoire ? (0/1)" ;
// 			cin >> pars.randomAntecedent;
// 		}else{
// 			pars.randomAntecedent = atoi(argv[8]);
// 		}
// 	}
// 	return move(pars);
// }

// //Fonctions de test

// // Test pgcd et ordre
// // Sortie attendue :
// // 15 35 5 7
// // 12 18 6 3
// // 7 35 7 5
// void testPgcdOrdre(){
// 	cout << 15 << " " << 35 << " " << gcd(15,35) << " " << ordre(35,15) << endl;
// 	cout << 12 << " " << 18 << " " << gcd(18,12) << " " << ordre(18,12) << endl;
// 	cout << 7 << " " << 35 << " " << gcd(7,35) << " " << ordre(35,7) << endl;
// }

// int main(int argc, char** argv)  // Programme principal 
// {
// 	// writeContainer(cout,invPoidsA(2,19,Automate::SUB),""," ","\n");
// 	// writeContainer(cout,invPoidsSuite(3,7,Automate::ADD),""," ","\n");
// 	srand(time(NULL)); // initialisation de rand
	
// 	Motif motif;
// 	time_t t1;
// 	time(&t1);
// 	srand(t1); 

// 	Parameters params = readCLParameters(argc,argv);

// 	int card = params.card; // donne le groupe permutatif 
// 	int choix = params.choix; // aléa ou déterministe? 
	
// 	int profondeur = params.profondeur; // la profondeur d'itération des différences finies
// 	int TailleF = params.TailleF; // la taille de la fenêtre que l'on observe. 
// 	int periode = params.periode;

// 	Automate automate = params.automate;
// 	AutoSide automateSide = params.automateSide;
// 	bool randomAntecedent = params.randomAntecedent;

// 	std::cout << "c'est parti!  " ;
// 	std::cout << std::endl; 
	
// 	vector<Suite<>> tab(profondeur,Suite<>(TailleF,0));
// 	//int ** tab = new int*[profondeur]; // le tableau
	
// 	if (choix == 1){
// 		motif = initSuitePeriodiqueRandom(periode,card);
// 	}else{
// 		motif=initSuitePeriodique(periode,card); 
// 	}
// 	tab.at(0) = motif.suite;

// 	// Test de oppose
// 	// writeContainer(cout, tab.at(0), ""," ","\n");
// 	// writeContainer(cout, oppose(tab.at(0),card), ""," ","\n");
// 	// return 0;


// 	// Calcul des différences finies mod card

// 	int tailleRegimeRep=-1;
// 	int tailleSigmaRep=-1;
// 	int shiftSigmaRep=-1;
// 	int firstRep=-1;

// 	if(randomAntecedent || automateSide == AutoSide::IMAGE){
// 		computeTab(tab,profondeur,card,automate,automateSide,randomAntecedent);
// 		afficheTab(tab, motif, card);  // afficheTab(vector<Suite<>>& tab,int periode, int profondeur, int TailleF, Suite<>& Motif,int N)
// 	}else{
// 		Suite<> current = motif.suite;
// 		for(int i = 0; i < profondeur; i++){
// 			afficheSuite(current,motif,card);
// 			for(int ante = 0; ante < card ; ante++){
// 				if(automate == Automate::ADD){
// 					for(int ante2 = 0; ante2 < card ; ante2++){
// 						int nextPeriode = periodeInv(2,{ante,ante2},card,current,automate);
// 						cout << ((nextPeriode == size(current)) ? termcolor::green : termcolor::reset)
// 							 << ante << "," << ante2 << ": " << nextPeriode << "\t";
// 					}
// 					cout << endl;
// 				} else{
// 					int nextPeriode = periodeInv(2,{ante},card,current,automate);
// 						cout << ((nextPeriode == size(current)) ? termcolor::green : termcolor::reset)
// 							 << ante << ": " << nextPeriode << "\t";
// 				}
// 			}
// 			cout << endl;
// 			current = computeAntecedent(current,automate,randomAntecedent,card);
// 		}
// 		afficheSuite(current,motif,card);
// 	}
// 	// cout << genLatex(tab,TailleF);
// 	// cout << PeriodeDeltaNiv2(card,tab[1][0],periode,tab[0]) << endl;

// 	// if(automateSide == AutoSide::IMAGE){
// 	// 	if(tailleRegimeRep == -1){
// 	// 		std::cout << "Régime reproductible non complet" << endl;
// 	// 	}else{
// 	// 		std::cout << "Taille du régime reproductible : " << tailleRegimeRep << endl;
// 	// 		if(tailleSigmaRep == -1){
// 	// 			std::cout << "Suite<> non shift-reproductible" << endl;
// 	// 		}else{
// 	// 			std::cout << "Suite<> " <<shiftSigmaRep<<"-reproductible en " << tailleSigmaRep << endl;
// 	// 		}
// 	// 		std::cout << "Décomposition :" << endl;
// 	// 		int repComponentIndex = firstRep+modulo(-firstRep,tailleRegimeRep);
// 	// 		std::cout << "Reproductible :";
// 	// 		for(int i=0;i<periode;i++){
// 	// 			std::cout << " " << tab[repComponentIndex][i];
// 	// 		}
// 	// 		std::cout << endl << "Réductible :" ;
// 	// 		for(int i=0;i<periode;i++){
// 	// 			std::cout << " " << modulo(tab[0][i] - tab[repComponentIndex][i],card);
// 	// 		}
// 	// 		std::cout << endl;
// 	// 	}
// 	// }
// 	//cin >> card;
// 	return 0;
// }

#include <iostream>

#include "grille.hh"
#include "modInt.hh"

int main(int argc, char** argv){
	int mod = 9;
	Grille<ModInt,true> grilleA({ModInt(4,mod),ModInt(2,mod),ModInt(1,mod),ModInt(-7,mod),ModInt(-2,mod),ModInt(-1,mod)},//,ModInt(4,mod),ModInt(1,mod),ModInt(2,mod)},
		Automate::ADD);
	// const int period0 = 4;
	// for(int j = 1;j < 32; j++){
	// 	grilleA.setAntecedent(Grille<ModInt,true>::Contrainte(0,ModInt(0,8)));
	
	// 	std::cout << std::endl;
	// 	//std::cout << (int) grilleA.at(j,4) << " ";		
	// }


	for(int i = 0; i< 80; i++){
		cout << (bigint) grilleA.at(0,i) << " ";
	}
	cout << endl;
	for(int j = 1;j < 1000; j++){
		grilleA.setAntecedent(Grille<ModInt,true>::Contrainte(0,ModInt(0,mod)));
		cout << j << endl;
		for(int i = 0; i< 80; i++){
			cout << (bigint) grilleA.at(j,i) << " ";
		}
		cout << endl;
		//cout << (int) grilleA.at(27-j,0) << endl ;

		// auto poidsA = grilleA.beta(27-j, 0);
		// auto poidsSuite = grilleA.theta(27-j, 0);
		// for(int k = 0; k < mod; k++){
		// 	int s = 0;
		// 	for(int m = 0; mod*m+k < size(poidsSuite); m++){
		// 		s+= poidsSuite.at(mod*m+k);
		// 	}
		// 	cout << s%mod << "(" << s << ") ";
		// }
		// cout << endl;
		// for(auto k:poidsSuite){
		// 	cout << k%mod << "("<< k << ") ";
		// }
		
		// for(int i = 0; i<80;i++){
		// 	if( grilleA.at(-j+1,i)!= grilleA.at(-j,i+1) - grilleA.at(-j,i) ){
		// 		cerr << "Bad value at index " << j << "," << i << endl;
		// 		auto poidsA = grilleA.beta(-j, i+1);
		// 		auto poidsSuite = grilleA.theta(-j, i+1);



		
		// 		cout << endl;
		// 		for(auto k:poidsSuite){
		// 			cout << k%9 << "("<< k << ") ";
		// 		}
		// 		cout << endl;
		// 		return 1;
		// 	}
		// }
		// auto poidsA = grilleA.beta(9-j, 0);
		// auto poidsSuite = grilleA.theta(9-j, 0);



		// for(auto i:poidsA){
		// 	cout << i%9 << "("<< i << ") ";
		// }
		// cout << endl;
		// for(auto i:poidsSuite){
		// 	cout << i%9 << "("<< i << ") ";
		// }
		// for(int k=0; k<4; k++){

		// }
		cout << endl << endl;
	}

	// auto poidsA = grilleA.beta(-10, 38);
	// auto poidsSuite = grilleA.theta(-10, 38);

	// for(auto i:poidsA){
	// 	cout << i%9 << "("<< i << ") ";
	// }
	// cout << endl;
	// for(auto i:poidsSuite){
	// 	cout << i%9 << "("<< i << ") ";
	// }
	// cout << endl;

	
	// for(int i = 0;i < 4; i++){
	// 	std::cout << (int) grilleA.at(1,i) << " ";		
	// }
	// std::cout << std::endl;
	// for(int i = 0;i < 3; i++){
	// 	std::cout << (int) grilleA.at(2,i) << " ";
	// }
	// std::cout << std::endl;

	// grilleA.setAntecedent(Grille<ModInt,false>::Contrainte(5,2));
	// for(int i = 0;i < 6; i++){
	// 	std::cout << (int) grilleA.at(-1,i) << " ";
	// }
	// std::cout << std::endl;

	// grilleA.setAntecedent(Grille<ModInt,false>::Contrainte(2,2));
	// for(int i = 0;i < 7; i++){
	// 	std::cout << (int) grilleA.at(-2,i) << " ";		
	// }
	// std::cout << std::endl;

	// try{
	// 	cout << (int) grilleA.at(-3,2) << endl;
	// }catch(out_of_range e){
	// 	cout << e.what() << endl;
	// }
	// try{
	// 	cout << (int) grilleA.at(242,643) << endl;
	// }catch(out_of_range e){
	// 	cout << e.what() << endl;
	// }

	std::cout << std::endl;
}