#!/bin/bash

period=8
card=4
prof=30
auto="+"


for i in `seq 1 500`
do
    echo `./a.out 1 $prof $card $period 1 $auto '-' 1 | grep -o -e "---.*" | cut -d" " -f2` >> ${period}_${card}_${prof}_$auto.csv
done