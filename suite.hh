#ifndef SUITE_HH
#define SUITE_HH

#include <vector>
#include <exception>
#include <functional>
using namespace std;

/** Classe pour représenter des suites périodiques
 * 
 */
template<typename V = int>
class Suite
{
//TODO: Ajouter les méthodes au fur et à mesure qu'elles sont nécessaires
public:
    typedef V value_type;
    typedef vector<value_type> BaseType;
private:
    /* data */
    BaseType data;
public:
    Suite() = default;
    template <class ForwardIterator> Suite(ForwardIterator begin, ForwardIterator end)
        :data(begin,end){};
    explicit Suite(size_t n)
        :data(n){};
    Suite(size_t n, value_type value)
        :data(n,value){};
    Suite(initializer_list<value_type> il)
        :data(il){};
    Suite(Suite const & that)
        :data(that.data){};
    Suite(Suite const && that)
        :data(move(that.data)){};
    ~Suite() = default;

    Suite & operator=(Suite const && that){
        data = move(that.data);
        return *this;
    }

    Suite & operator=(Suite const & that){
        data = move(that.data);
        return *this;
    }

    value_type& operator[](size_t index){
        return data[index%data.size()];
    }
    value_type const & operator[](size_t index) const{
        return data[index%data.size()];
    }

    value_type & at(size_t index){
        if(data.size()==0){
            throw out_of_range("Attempted to index empty Suite");
        }
        return data.at(index%data.size());
    }
    
    value_type const & at(size_t index) const{
        if(data.size()==0){
            throw out_of_range("Attempted to index empty Suite");
        }
        return data.at(index%data.size());
    }

    size_t size() const{
        return data.size();
    }

    bool empty() const{
        return data.empty();
    }

    class iterator{
        friend Suite;
        friend class const_iterator;
    private:
        int currentIndex;
        Suite * container;
        iterator(size_t _currentIndex, Suite & _container)
            :currentIndex(_currentIndex),container(&_container){};

    public:
        typedef std::random_access_iterator_tag iterator_category;
        typedef Suite::value_type value_type;
        typedef value_type* pointer;
        typedef value_type& reference;
        typedef int difference_type;

        iterator() = default;
        iterator(iterator const & other) = default;
        iterator(iterator && other) = default;
        iterator& operator=(iterator const & other) = default;
        iterator& operator=(iterator && other) = default;

        iterator operator+(int steps) const{
            return iterator(currentIndex + steps,*container);
        }

        //FIXME: operator-(const_iterator,const_iterator) n'est pas pris en compte dans le lookup de -
        // celui-ci le masque
        iterator operator-(int steps) const{
            return iterator(currentIndex - steps,*container);
        }


        iterator& operator+=(int steps){
            currentIndex+=steps;
            return *this;
        }

        iterator& operator++(){
            ++currentIndex;
            return *this;
        }

        reference operator*(){
            return container->at(currentIndex % container->size());
        }

    };

    iterator begin(){
        return iterator(0,*this);
    }

    iterator end(){
        return iterator(this->size(),*this);
    }

    class const_iterator{
        friend Suite;
        
    private:
        int currentIndex;
        Suite const * container;
        const_iterator(size_t _currentIndex, Suite const & _container)
            :currentIndex(_currentIndex),container(&_container){};

    public:
        typedef std::random_access_iterator_tag iterator_category;
        typedef Suite::value_type value_type;
        typedef value_type const * pointer;
        typedef value_type const & reference;
        typedef int difference_type;
        friend difference_type operator-(Suite::const_iterator a,Suite::const_iterator b);
        friend bool operator!=(const_iterator a,const_iterator b){
            return a.currentIndex != b.currentIndex;
        };
        friend bool operator==(const_iterator a,const_iterator b){
            return !(a!=b);
        };


        const_iterator() = default;
        const_iterator(const_iterator const & other) = default;
        const_iterator(const_iterator && other) = default;
        const_iterator& operator=(const_iterator const & other) = default;
        const_iterator& operator=(const_iterator && other) = default;

        const_iterator(iterator const & other)
            :currentIndex(other.currentIndex),container(other.container){};

        const_iterator operator+(int steps) const{
            return const_iterator(currentIndex + steps,*container);
        }
        
        const_iterator operator-(int steps) const{
            return const_iterator(currentIndex - steps,*container);
        }

        friend typename const_iterator::difference_type operator-(const_iterator a, const_iterator b){
            return a.currentIndex-b.currentIndex;
        };

        const_iterator& operator+=(int steps){
            currentIndex+=steps;
            return *this;
        }

        const_iterator& operator++(){
            ++currentIndex;
            return *this;
        }

        reference operator*(){
            return container->at(currentIndex % container->size());
        }

    };

    const_iterator begin() const{
        return const_iterator(0,*this);
    }

    const_iterator end() const{
        return const_iterator(this->size(),*this);
    }

    const_iterator cbegin() const{
        return const_iterator(0,*this);
    }

    const_iterator cend() const{
        return const_iterator(this->size(),*this);
    }

    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    reverse_iterator rbegin()
    {
        return reverse_iterator(end());
    }

    reverse_iterator rend()
    {
        return reverse_iterator(begin());
    }
    
    const_reverse_iterator rbegin() const
    {
        return const_reverse_iterator(end());
    }

    const_reverse_iterator rend() const
    {
        return const_reverse_iterator(begin());
    }

    // Réduit la séquence à une seule période
    void reduce(){
        //TODO: Utiliser la surcharge pour se servir d'un meilleur searcher (pas trouvé actuellement)
        auto found = search(next(cbegin()),next(cend()+size()),cbegin(),cend());
        data.erase(next(data.begin(),distance(cbegin(),found)),data.end());
        return;
    }

};

// template <typename const_iterator>
// bool operator!=(const_iterator a,const_iterator b){
//     return a.currentIndex != b.currentIndex;
// }

// template <typename const_iterator>
// bool operator==(const_iterator a,const_iterator b){
//     return !(a!=b);
// }


// C++ devrait faire une conversion implicite, mais non...
bool operator!=(Suite<int>::iterator a,Suite<int>::iterator b){
    return Suite<int>::const_iterator(a) != Suite<int>::const_iterator(b);
}

typename Suite<int>::iterator::difference_type operator-(
        Suite<int>::iterator a,
        Suite<int>::iterator b){
    return Suite<int>::const_iterator(a) - Suite<int>::const_iterator(b);
}

#endif