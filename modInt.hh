#ifndef MODINT_H
#define MODINT_H

#include <stdexcept>
#include "bigint.hh"

// Calcule le "vrai" modulo (positif même si le dividende est négatif) 
// Encore négatif si le diviseur est négatif
bigint modulo(bigint dividende, bigint diviseur) {
	return ((dividende % diviseur) + diviseur) % diviseur;
}

/** Classe modélisant les entiers modulaires
 * 
 */
class ModInt{
private:
    bigint value;
    int mod; // Si mod=0, alors ModInt est simplement un entier

    static int commonMod(ModInt const & a, ModInt const & b){
        if(a.mod==b.mod){
            return a.mod;
        }else if (a.mod==0){
            return b.mod;
        }else if (b.mod==0){
            return a.mod;
        }else{
            throw std::logic_error("Opération impossible entre entiers de modulo différents");
        }
    }
public:
    ModInt(bigint n=0,int mod=0)
        :value(mod!=0 ? modulo(n,mod) : n),mod(mod)
    {}

    explicit operator int(){
        return int(value);
    }

    explicit operator bigint(){
        return value;
    }

    friend ModInt operator+(ModInt const & a, ModInt const & b){
        return ModInt(a.value+b.value,commonMod(a,b));
    }

    friend ModInt operator-(ModInt const & a, ModInt const & b){
        return ModInt(a.value-b.value,commonMod(a,b));
    }

    ModInt operator-() const {
        return ModInt(-value,mod);
    }

    friend ModInt operator*(ModInt const & a, ModInt const & b){
        return ModInt(a.value*b.value,commonMod(a,b));
    }

    friend bool operator==(ModInt const & a, ModInt const & b){
        return (a-b).value == 0;
    }

    friend bool operator!=(ModInt const & a, ModInt const & b){
        return (a-b).value != 0;
    }
};

#endif