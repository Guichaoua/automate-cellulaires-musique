#ifndef MISC_HH
#define MISC_HH

#include <algorithm>
#include "bigint.hh"

/** Calcule un coefficient binomial
 * Attention : Overflow 
 */
bigint binomialCoeff(int n, int k) 
{ 
	// Since C(n, k) = C(n, n-k) 
	k = std::min(k,n - k); 
	if (k<0){
		return 0;
	}
	bigint res = 1;   
    // Calculate value of [n * (n-1) *---* (n-k+1)] / [k * (k-1) *----* 1] 
    for (int i = 0; i < k; ++i) 
    { 
        res *= (n - i); 
        res /= (i + 1); // No remainder because we multiplied by i+1 consecutive integers
    } 
  
    return res; 
} 

#endif