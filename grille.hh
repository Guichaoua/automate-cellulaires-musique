#ifndef GRILLE_HH
#define GRILLE_HH

#include "suite.hh"
#include "misc.hh"
#include <algorithm>
#include <functional>
#include <numeric>
#include <stdexcept>

#include <iostream>

using namespace std;

enum class Automate{
		ADD,SUB
};


template<typename value_type = bigint, bool cyclic = true>
class Grille{
public:
    struct Contrainte{// Une contrainte sur le choix des valeurs des antécédents
        int colonne; // Indique la position de la contrainte
        value_type valeur; // Indique la valeur souhaitée à la position donnée

        Contrainte(int colonne, value_type valeur):
            colonne(colonne), valeur(valeur)
        {};
    };
    typedef typename std::conditional<cyclic, Suite<value_type>, std::vector<value_type>>::type BaseSuite;

private:
    BaseSuite X0; 
    Automate automate;
    vector<Contrainte> Yc; // Les paires (colonne, valeur) des contraintes sur les antécédents
    vector<value_type> Y0; // Les valeurs en 0 des antécédents

private:
    /** Donne les coefficients de X_0 pour la valeur en X_n(i)
     * 
     */
    vector<bigint> theta(int n, int i) const {
        if(n<0){
            n=-n;
            vector<bigint> poids(max(i-n+1,0));
	        generate(begin(poids),end(poids),[&,j=0]() mutable {
                // Le signe est toujours positif si on est sur delta, sinon il dépend de la parité de periode, profondeur et j
                bigint signe = ((automate == Automate::ADD) && (i+j+n)%2) ? -1 : 1;
                return signe * binomialCoeff(i-j++-1,n-1);
            });
            return poids;
        }else{
            vector<bigint> poids(n+1);
            generate(rbegin(poids),rend(poids),[&,j=0]() mutable {
                // Le signe est toujours positif si on est sur delta, sinon il dépend de la parité de periode et j
                bigint signe = (automate == Automate::SUB) && j%2 ? -1 : 1;
                return signe * binomialCoeff(n,j++);
            });
            return poids;
        }
    }

    /** Donne les coefficients de Y_0 pour la valeur en X_n(i)
     * 
     */
    vector<bigint> beta(int n, int i) const {
        if(n<0){
            n=-n;
            vector<bigint> poids(n);
            generate(rbegin(poids),rend(poids),[&,j=0]() mutable {
                // Le signe est toujours positif si on est sur delta, sinon il dépende de la parité de periode et j
                bigint signe = (automate == Automate::ADD) && (i+j)%2 ? -1 : 1;
                return signe * binomialCoeff(i,j++);
            });
            return move(poids);
        }else{
            return vector<bigint>(); // No betas when moving forward in time
        }
    }

    /** Vérifie les préconditions pour accéder à la valeur en (ligne, colonne)
     * 
     */
    void assertDetermination(int ligne, int colonne) const {
        if(-ligne > static_cast<int>(size(Y0))){
            throw out_of_range("Pas assez d'antécédents fixés");
        }else if(!cyclic){
            if(colonne < 0 )
                throw out_of_range("Indices négatifs non définis pour les suites finies");
            if(colonne + ligne >= static_cast<int>(size(X0)))
                throw out_of_range("Suite trop courte pour cette position");
        }
    }

public:
    /** Construit une grille à partir d'une suite et d'un automate
     * 
     */
    Grille(BaseSuite init, Automate automate):
        X0(init), automate(automate)
    {};

    /** Construit une grille à partir d'une suite d'un automate et d'une liste de contraintes
     * 
     */
    Grille(BaseSuite init, Automate automate, vector<Contrainte> contraintes)
        :Grille(init, automate)
    {
        for(Contrainte c : contraintes){
            setAntecedent(c);
        }
    }

    /** Renvoie la valeur X_ligne(colonne)
     * 
     */
    value_type at(int ligne, int colonne) const{
        assertDetermination(ligne, colonne);
        if(ligne<0){
            auto poidsA = beta(ligne, colonne);
            auto poidsSuite = theta(ligne, colonne);
            value_type val = std::inner_product(begin(poidsA),end(poidsA),begin(Y0),value_type(0))
					       + std::inner_product(begin(poidsSuite),end(poidsSuite),begin(X0),value_type(0));
            return val;
        }else{
            auto poidsSuite = theta(ligne, colonne);
            value_type val = std::inner_product(begin(poidsSuite),end(poidsSuite),begin(X0)+colonne,value_type(0));
            return val;
        }
    }


    /** Fixe l'antécédent qui vaut "valeur" à la position "colonne"
     * 
     */
    void setAntecedent(Contrainte contrainte){
        Yc.push_back(contrainte);
        
        // Calcule la valeur si l'antécédent en 0 était 0
        value_type val0;
        Y0.push_back(bigint(0));
        try{
            val0 = at(-size(Yc),contrainte.colonne);
        }catch(out_of_range e){
            Y0.pop_back();
            throw e;
        }
        Y0.pop_back();

        int signe = beta(-size(Yc),contrainte.colonne).back().sign;
        //assert(abs(signe)<=1);

        Y0.push_back((contrainte.valeur-val0)* bigint(signe));
    }

    
    /** Efface le dernier antécédent
     * 
     */
    void unsetAntecedent(){
        Yc.pop_back();
        Y0.pop_back();
    };

    /** Modifie la suite initiale, en conservant les contraintes
     * 
     */
    void updateSuite(BaseSuite suite);
};

#endif